OBJS = main.o vector.o matrix.o multiplymv.o multiplymm.o

prog1 : $(OBJS)
	gcc -o prog1 $(OBJS) -pthread

main.o : main.c 
	gcc -c main.c -pthread

vector.o : vector.c vector.h
	gcc -c vector.c -pthread

matrix.o : matrix.c matrix.h
	gcc -c matrix.c -pthread

multiplymm.o : multiplymm.c matrix.h
	gcc -c multiplymm.c -pthread

multiplymv.o : multiplymv.c matrix.h vector.h
	gcc -c multiplymv.c -pthread
clean :
	rm $(OBJS)
